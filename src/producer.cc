#include "producer.h"

using namespace std;
namespace tardigrade
{
	TardigradeProducer::TardigradeProducer() :
		worker_thread_(nullptr)
	{
	}

	TardigradeProducer::~TardigradeProducer()
	{
	}

	void TardigradeProducer::Start()
	{
		function<void()> start_func = bind(&TardigradeProducer::StartImplementation, this);
		worker_thread_ = new thread(start_func);
	}

	void TardigradeProducer::JoinWorkerThread()
	{
		if (worker_thread_ && worker_thread_->joinable())
			worker_thread_->join();
		if (worker_thread_)
			delete worker_thread_;
		worker_thread_ = nullptr;
	}

	void TardigradeProducer::Publish(const EventMessage& message)
	{
		Publish(0, message);
	}

	void TardigradeProducer::Publish(const int index, const EventMessage& message)
	{
		transport_node_->Publish(producer_topics_[index], message);
	}
};
