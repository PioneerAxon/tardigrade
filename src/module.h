#ifndef __TARDIGRADE_MODULE_H__
#define __TARDIGRADE_MODULE_H__

#include <string>
#include <vector>

#include <ignition/transport.hh>

using namespace std;
namespace tardigrade
{
	enum class ModuleType
	{
		tUnknown,
		tProducer,
		tConsumer,
		tOperator,
	};

	class TardigradeModule;
	typedef TardigradeModule* new_module_t();
	typedef void destroy_module_t(TardigradeModule* module);

	class TardigradeModule
	{
		public:
			TardigradeModule();
			~TardigradeModule();
			void Initialize(void* libdl_object, string arguments, vector<string> producer_topics, vector<string>consumer_topics, string _namespace = "default_namespace", string partition = "tardigrade");
			void* Terminate();

			virtual ModuleType GetModuleType() = 0;
			virtual void InitializeImplementation(string arguments) = 0;
			virtual void TerminateImplementation() = 0;


		public:
			new_module_t* new_module_func_;
			destroy_module_t* destroy_module_func_;

		private:
			void* libdl_object_;
			string namespace_;
			string partition_;

		protected:
			ignition::transport::Node* transport_node_;
			vector<string> producer_topics_;
			vector<string> consumer_topics_;
	};

};

#endif //__TARDIGRADE_MODULE_H__
