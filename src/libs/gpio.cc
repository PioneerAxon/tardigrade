#include <glog/logging.h>
#include <map>
#include <functional>

#include "gpio.h"

#define CALLBACK_FUNCTION(PIN) \
	static void callback_function_ ## PIN () \
	{ \
		CallbackHandler(PIN); \
	}

#define REGISTER_CALLBACK(PIN) \
	wiringpi_callbacks_[PIN] = callback_function_ ## PIN ;

#define REPEAT(X) \
	X(0) \
	X(1) \
	X(2) \
	X(3) \
	X(4) \
	X(5) \
	X(6) \
	X(7) \
	X(8) \
	X(9) \
	X(10) \
	X(11) \
	X(12) \
	X(13) \
	X(14) \
	X(15) \
	X(16) \
	X(17) \
	X(18) \
	X(19) \
	X(20) \
	X(21) \
	X(22) \
	X(23) \
	X(24) \
	X(25) \
	X(26) \
	X(27) \
	X(28) \
	X(29) \
	X(30) \
	X(31) \
	X(32) \
	X(33) \
	X(34) \
	X(35) \
	X(36) \
	X(37) \
	X(38) \
	X(39)

static bool initialized_ = false;
static std::map<int, tardigrade::GPIOPinMode> pin_mode_map_;
static std::map<int, bool> pin_value_map_;
static std::map<int, std::function<void()>> pin_callback_map_;
static void (*wiringpi_callbacks_[40])();

namespace tardigrade
{
	namespace GPIO
	{
		REPEAT(CALLBACK_FUNCTION)

		void Init(GPIOPinNumberingMode mode)
		{
			if (initialized_)
				return;

			LOG(INFO) << "Initializing WiringPi in " << mode << " mode";

			initialized_ = true;

#ifdef HAS_WIRINGPI
			REPEAT(REGISTER_CALLBACK);
			switch(mode)
			{
				case tPhysicalMode:
					CHECK_EQ(wiringPiSetupPhys(), 0);
					return;
				case tBroadcomMode:
					CHECK_EQ(wiringPiSetupGpio(), 0);
					return;
				case tWiringPiMode:
					CHECK_EQ(wiringPiSetup(), 0);
					return;
			}
#else
			LOG(ERROR) << "The GPIO library was compiled without WiringPi. Ignoring the initialization call.";
#endif
		}

		void SetPinMode(int pin, GPIOPinMode mode)
		{
			LOG(INFO) << "Setting mode " << mode << " on pin " << pin;
#ifdef HAS_WIRINGPI
			CHECK(initialized_);
			pinMode(pin, mode);
#else
			LOG(FATAL) << "The GPIO library was compiled without WiringPi";
#endif
			pin_mode_map_[pin] = mode;
		}

		void SetInputPullMode(int pin, GPIOPullUpDownMode mode)
		{
			LOG(INFO) << "Setting pull mode " << mode << " on pin " << pin;
#ifdef HAS_WIRINGPI
			CHECK(initialized_);
			CHECK_EQ(pin_mode_map_[pin], tInputPin);
			pullUpDnControl(pin, mode);
#else
			LOG(FATAL) << "The GPIO library was compiled without WiringPi";
#endif
		}

		void Write(int pin, bool value)
		{
			VLOG(1) << "Setting value " << value << " on pin " << pin;
#ifdef HAS_WIRINGPI
			CHECK(initialized_);
			CHECK_EQ(pin_mode_map_[pin], tOutputPin) << "Attempt to write on pin " << pin << ", which is in " << pin_mode_map_[pin] << " mode";
			digitalWrite(pin, value ? 1 : 0);
#else
			LOG(FATAL) << "The GPIO library was compiled without WiringPi";
#endif
			pin_value_map_[pin] = value;
		}

		bool Read(int pin)
		{
			bool ret = false;
			VLOG(0) << "Reading value on pin " << pin;
#ifdef HAS_WIRINGPI
			CHECK(initialized_);
			CHECK_EQ(pin_mode_map_[pin], tInputPin) << "Attempt to read on pin " << pin << ", which is in " << pin_mode_map_[pin] << " mode";
			ret = digitalRead(pin) == 1;
			pin_value_map_[pin] = ret;
#else
			LOG(FATAL) << "The GPIO library was compiled without WiringPi";
#endif
			return ret;
		}

		void RegisterCallback(int pin, GPIOCallbackEdge edge, std::function<void()> callback)
		{
			LOG(INFO) << "Registering callback on pin " << pin << " for edge type " << edge;
#ifdef HAS_WIRINGPI
			CHECK(initialized_);
			CHECK(callback);
			CHECK_EQ(pin_mode_map_[pin], tInputPin) << "Attempt to register callback on pin " << pin << " which is in " << pin_mode_map_[pin] << " mode";
			CHECK(pin_callback_map_.find(pin) == pin_callback_map_.end()) << "Attempt to register callback on a pin " << pin << " which is already registered";
			pin_callback_map_[pin] = callback;
			CHECK(wiringpi_callbacks_[pin]);
			wiringPiISR(pin, edge, wiringpi_callbacks_[pin]);
#else
			LOG(FATAL) << "The GPIO library was compiled without WiringPi";
#endif
		}

		static void CallbackHandler(int pin)
		{
			if (pin_callback_map_.find(pin) != pin_callback_map_.end())
			{
				(pin_callback_map_[pin])();
			}
		}

		void UnregisterCallback(int pin)
		{
#ifdef HAS_WIRINGPI
			CHECK(initialized_);
			CHECK_EQ(pin_mode_map_[pin], tInputPin) << "Attempt to unregister callback on pin " << pin << " which is in " << pin_mode_map_[pin] << " mode";
			CHECK(pin_callback_map_.find(pin) != pin_callback_map_.end()) << "Attempt to unregister callback on a pin " << pin << " which is not registered yet";
			pin_callback_map_.erase(pin);
#else
			LOG(FATAL) << "The GPIO library was compiled without WiringPi";
#endif
		}

	};
};
