#ifndef __TARDIGRADE_GPIO_H__
#define __TARDIGRADE_GPIO_H__

#ifdef HAS_WIRINGPI
#include <wiringPi.h>
#endif

namespace tardigrade
{
	enum GPIOPinNumberingMode
	{
		tPhysicalMode,
		tBroadcomMode,
		tWiringPiMode,
	};

	enum GPIOPinMode
	{
#ifdef HAS_WIRINGPI
		tInputPin = INPUT,
		tOutputPin = OUTPUT,
		tPwmOutputPin = PWM_OUTPUT,
#else
		tInputPin,
		tOutputPin,
		tPwmOutputPin,
#endif
	};

	enum GPIOPullUpDownMode
	{
#ifdef HAS_WIRINGPI
		tPullNoneMode = PUD_OFF,
		tPullDownMode = PUD_DOWN,
		tPullUpMode = PUD_UP,
#else
		tPullNoneMode,
		tPullDownMode,
		tPullUpMode,
#endif
	};

	enum GPIOCallbackEdge
	{
#ifdef HAS_WIRINGPI
		tCallbackEdgeFalling = INT_EDGE_FALLING,
		tCallbackEdgeRising = INT_EDGE_RISING,
		tCallbackEdgeBoth = INT_EDGE_BOTH,
#else
		tCallbackEdgeFalling,
		tCallbackEdgeRising,
		tCallbackEdgeBoth,
#endif
	};

	namespace GPIO
	{
		void Init(GPIOPinNumberingMode mode);

		void SetPinMode(int pin, GPIOPinMode mode);

		void SetInputPullMode(int pin, GPIOPullUpDownMode mode);

		void Write(int pin, bool value);

		bool Read(int pin);

		void RegisterCallback(int pin, GPIOCallbackEdge edge, std::function<void()> callback);

		void UnregisterCallback(int pin);

		static void CallbackHandler(int pin);
	};
};

#endif
