#include <glog/logging.h>

#include "event-message.pb.h"

#include "module.h"
#include "producer.h"
#include "consumer.h"
#include "operator.h"

namespace tardigrade
{
	TardigradeModule::TardigradeModule() :
		new_module_func_(nullptr),
		destroy_module_func_(nullptr),
		libdl_object_(nullptr),
		namespace_(""),
		partition_(""),
		transport_node_(nullptr)
	{
	}

	TardigradeModule::~TardigradeModule()
	{
		CHECK(!libdl_object_);
		if (transport_node_)
			delete transport_node_;
	}

	void TardigradeModule::Initialize(void* libdl_object, string arguments, vector<string> producer_topics, vector<string>consumer_topics, string _namespace, string partition)
	{
		CHECK(libdl_object);
		libdl_object_ = libdl_object;

		producer_topics_ = producer_topics;
		consumer_topics_ = consumer_topics;
		namespace_ = _namespace;
		partition_ = partition;

		transport_node_ = new ignition::transport::Node(partition_, namespace_);

		InitializeImplementation(arguments);
		if (GetModuleType() == ModuleType::tProducer || GetModuleType() == ModuleType::tOperator)
		{
			for (int ll = 0; ll < producer_topics_.size(); ++ll)
			{
				CHECK(transport_node_->Advertise(producer_topics_[ll]));
			}
			if (GetModuleType() == ModuleType::tProducer)
			{
				((TardigradeProducer*)this)->Start();
			}
		}
		if (GetModuleType() == ModuleType::tConsumer || GetModuleType() == ModuleType::tOperator)
		{
			for (int ll = 0; ll < consumer_topics_.size(); ++ll)
			{
				if (GetModuleType() == ModuleType::tConsumer)
					CHECK((transport_node_->Subscribe<TardigradeConsumer, EventMessage>(consumer_topics_[ll], &TardigradeConsumer::Callback, ((TardigradeConsumer*)this))));
				if (GetModuleType() == ModuleType::tOperator)
					CHECK((transport_node_->Subscribe<TardigradeOperator, EventMessage>(consumer_topics_[ll], &TardigradeOperator::Callback, ((TardigradeOperator*)this))));
			}
		}
	}

	void* TardigradeModule::Terminate()
	{
		void* libdl_object = libdl_object_;
		libdl_object_ = nullptr;
		TerminateImplementation();
		if (GetModuleType() == ModuleType::tProducer)
		{
			((TardigradeProducer*)this)->JoinWorkerThread();
		}
		return libdl_object;
	}
}
