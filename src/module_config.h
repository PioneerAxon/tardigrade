#ifndef __TARDIGRADE_MODULE_CONFIG_H__
#define __TARDIGRADE_MODULE_CONFIG_H__

#include <string>
#include <vector>
#include <iostream>
#include <gflags/gflags.h>

#include "module.h"

using namespace std;
namespace tardigrade
{
	struct ModuleConfig
	{
		string module_name_;
		string arguments_;
		ModuleType type_;
		string module_path_;
		string partition_;
		string namespace_;
		vector<string> consumer_topics_;
		vector<string> producer_topics_;

		ModuleConfig(string module_name, string arguments, string partition, string _namespace, ModuleType type, string module_path = "");
		ModuleConfig();
		~ModuleConfig();

		void SetPath(string path = "");

	};
};

#endif //__TARDIGRADE_MODULE_CONFIG_H__
