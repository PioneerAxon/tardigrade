#ifndef __TARDIGRADE_MODULE_LOADER_H__
#define __TARDIGRADE_MODULE_LOADER_H__

#include <string>
#include <list>

#include <glog/logging.h>

#include "module.h"
#include "module_config.h"

using namespace std;

namespace tardigrade
{
	class ModuleLoader
	{
		public:
			ModuleLoader();
			~ModuleLoader();

			void LoadAll(const list<ModuleConfig>& modules);
			void UnloadAll ();

		private:
			TardigradeModule* LoadModule(const ModuleConfig& config);
			void UnloadModule(TardigradeModule* module);

		private:
			list<TardigradeModule*> loaded_modules_;
	};
};

#endif //__TARDIGRADE_MODULE_LOADER_H__
