#include <unistd.h>
#include <assert.h>
#include <sys/wait.h>
#include <iostream>
#include <chrono>
#include <thread>

#include <glog/logging.h>

#include "tardigrade_monitor.h"
#include "tardigrade_service.h"

using namespace std;
namespace tardigrade
{

	TardigradeMonitor::TardigradeMonitor()
	{
	}

	TardigradeMonitor::~TardigradeMonitor()
	{
	}

	void TardigradeMonitor::Run()
	{
		int exit_status;
		do
		{
			exit_status = RunService();
			LOG(ERROR) << "Service exited with exit status : " << exit_status;
			LOG_IF(ERROR, exit_status != 0) << "Restarting service..";
			if (exit_status) {
				std::this_thread::sleep_for (std::chrono::seconds(1));
			}
		} while(exit_status);
	}

	void TardigradeMonitor::SignalHandler(int signal)
	{
	}

	int TardigradeMonitor::RunService()
	{
		pid_t service_pid;
		int service_status;

		service_pid = fork();
		if (service_pid)
		{
			signal(SIGINT, TardigradeMonitor::SignalHandler);
			LOG(INFO) << "Service running with pid : " << service_pid;
			assert(wait(&service_status) == service_pid);
		}
		else
		{
			Tardigrade tardigrade_service;
			tardigrade_service.Run();
		}
		return service_status;
	}
};
