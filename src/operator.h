#ifndef __TARDIGRADE_OPERATOR_H__
#define __TARDIGRADE_OPERATOR_H__

#include <string>

#include "event-message.pb.h"

#include "module.h"

namespace tardigrade
{
	class TardigradeOperator : public TardigradeModule
	{
		public:
			TardigradeOperator();
			~TardigradeOperator();

			ModuleType GetModuleType()
			{
				return ModuleType::tOperator;
			}

			virtual void InitializeImplementation(std::string arguments) = 0;
			virtual void TerminateImplementation() = 0;
			virtual void Callback(const string&, const EventMessage&) = 0;

			void Publish(const int index, const EventMessage& message);
			void Publish(const EventMessage& message);
	};
};

#endif //__TARDIGRADE_CONSUMER_H__
