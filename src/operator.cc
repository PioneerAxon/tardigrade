#include "operator.h"

namespace tardigrade
{
	TardigradeOperator::TardigradeOperator()
	{
	}

	TardigradeOperator::~TardigradeOperator()
	{
	}

	void TardigradeOperator::Publish(const EventMessage& message)
	{
		Publish(0, message);
	}

	void TardigradeOperator::Publish(const int index, const EventMessage& message)
	{
		transport_node_->Publish(producer_topics_[index], message);
	}
};
