#include <iostream>
#include <string>
#include <functional>
#include <chrono>

#include <glog/logging.h>

#include "producer.h"
#include "event-message.pb.h"
#include "gpio.h"

using namespace std;
using namespace tardigrade;
class GPIOCallback : public TardigradeProducer {

	public:
		virtual void InitializeImplementation(string args)
		{
			LOG(INFO) << "Initializing GPIOCallback with args : " << args;
			try
			{
				pin_ = stoi (args);
				CHECK(pin_ > 0 && pin_ <= 40);
			}
			catch(...)
			{
				LOG(FATAL) << "Unable to parse argument \"" << args << "\" to a physical pin number";
			}
			LOG(INFO) << "Registering physical pin " << pin_ << " as output device";
			terminate_ = false;
			pin_status_ = false;
			last_callback_counter_ = 0;

			GPIO::SetPinMode(pin_, tInputPin);
			GPIO::SetInputPullMode(pin_, tPullDownMode);
			callback_ = std::bind(&GPIOCallback::Callback, this);
			GPIO::RegisterCallback(pin_, tCallbackEdgeFalling, callback_);
		}

		virtual void Callback()
		{
			pin_status_ = true;
			last_callback_counter_ = 1;
		}

		virtual void StartImplementation()
		{
			LOG(INFO) << "Running GPIOCallbacker";
			while (!terminate_)
			{
				this_thread::sleep_for(std::chrono::milliseconds(500));
				if(pin_status_)
				{
					pin_status_ = false;
					EventMessage message;
					message.set_binary_payload(true);

					Publish(message);
					continue;
				}
				if (last_callback_counter_)
					last_callback_counter_++;
				if (last_callback_counter_ == 4)
				{
					EventMessage message;
					message.set_binary_payload(false);

					Publish(message);
					last_callback_counter_ = 0;
				}
			}
		}

		virtual void TerminateImplementation()
		{
			LOG(INFO) << "Terminating GPIOCallback";
			terminate_ = true;
			GPIO::UnregisterCallback(pin_);
		}
	private:
		bool terminate_;
		int pin_;
		std::function<void()> callback_;
		bool pin_status_;
		int last_callback_counter_;
};
extern "C" TardigradeModule* new_module()
{
	return new GPIOCallback;
}

extern "C" void destroy_module(TardigradeModule* consumer)
{
	delete consumer;
}


