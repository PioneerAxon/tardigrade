#include <iostream>
#include <string>

#include <glog/logging.h>

#include "consumer.h"
#include "event-message.pb.h"
#include "gpio.h"

using namespace std;
using namespace tardigrade;
class GPIOWrite : public TardigradeConsumer {

	public:
		virtual void InitializeImplementation(string args)
		{
			LOG(INFO) << "Initializing GPIOWrite with args : " << args;
			try
			{
				pin_ = stoi (args);
				CHECK(pin_ > 0 && pin_ <= 40);
			}
			catch(...)
			{
				LOG(FATAL) << "Unable to parse argument \"" << args << "\" to a physical pin number";
			}
			LOG(INFO) << "Registering physical pin " << pin_ << " as output device";

			GPIO::SetPinMode(pin_, tOutputPin);
			GPIO::Write(pin_, false);
		}

		virtual void Callback(const string& topic, const EventMessage& message)
		{
			if (message.has_binary_payload())
			{
				GPIO::Write(pin_, message.binary_payload());
			}
		}

		virtual void TerminateImplementation()
		{
			LOG(INFO) << "Terminating GPIOWrite";
			GPIO::SetPinMode(pin_, tInputPin);
		}
	private:
		int pin_;
};
extern "C" TardigradeModule* new_module()
{
	return new GPIOWrite;
}

extern "C" void destroy_module(TardigradeModule* consumer)
{
	delete consumer;
}


