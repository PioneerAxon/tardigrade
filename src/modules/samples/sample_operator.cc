#include <iostream>
#include <string>

#include <glog/logging.h>

#include "operator.h"
#include "event-message.pb.h"

using namespace std;
using namespace tardigrade;
class SampleOperator : public TardigradeOperator {

	public:
		virtual void InitializeImplementation(string args)
		{
			LOG(INFO) << "Initializing SampleOperator with args : " << args;

			// Do more initialization here.
		}

		virtual void Callback(const string& topic, const EventMessage& message)
		{
			LOG(INFO) << "Callback to SampleOperator. Topic:" << topic << " Message:" <<message.ShortDebugString();
			if (message.has_binary_payload())
				state_map_ [topic] = message.binary_payload();
			bool state = state_map_.size() == 0 ? false : true;
			for (auto& x: state_map_)
			{
				state = state && x.second;
			}

			EventMessage result;
			result.set_binary_payload(state);
			Publish(result);
		}

		virtual void TerminateImplementation()
		{
			LOG(INFO) << "Terminating SampleOperator";
			//Start handling the message here
		}

	private:
		map<string, bool> state_map_;
};
extern "C" TardigradeModule* new_module()
{
	return new SampleOperator;
}

extern "C" void destroy_module(TardigradeModule* consumer)
{
	delete consumer;
}
