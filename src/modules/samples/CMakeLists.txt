include_directories(../../)

add_library(sample_producer SHARED sample_producer.cc)
add_library(sample_consumer SHARED sample_consumer.cc)
add_library(sample_operator SHARED sample_operator.cc)

set_target_properties(sample_producer PROPERTIES PREFIX "")
set_target_properties(sample_consumer PROPERTIES PREFIX "")
set_target_properties(sample_operator PROPERTIES PREFIX "")
