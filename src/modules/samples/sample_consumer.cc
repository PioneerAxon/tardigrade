#include <iostream>
#include <string>

#include <glog/logging.h>

#include "consumer.h"
#include "event-message.pb.h"

using namespace std;
using namespace tardigrade;
class SampleConsumer : public TardigradeConsumer {

	public:
		virtual void InitializeImplementation(string args)
		{
			LOG(INFO) << "Initializing SampleConsumer with args : " << args;
			// Do more initialization here.
		}

		virtual void Callback(const string& topic, const EventMessage& message)
		{
			LOG(INFO) << "Callback to SampleConsumer. Topic:" << topic << " Message:" << message.ShortDebugString();
			//Start handling the message here
		}

		virtual void TerminateImplementation()
		{
			LOG(INFO) << "Terminating SampleConsumer";
			//Start handling the message here
		}
};
extern "C" TardigradeModule* new_module()
{
	return new SampleConsumer;
}

extern "C" void destroy_module(TardigradeModule* consumer)
{
	delete consumer;
}

