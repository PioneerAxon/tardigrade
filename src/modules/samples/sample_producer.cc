#include <iostream>
#include <string>
#include <chrono>

#include <glog/logging.h>

#include "producer.h"

using namespace std;
using namespace tardigrade;
class SampleProducer : public TardigradeProducer {

	public:
		virtual void InitializeImplementation(string args)
		{
			LOG(INFO) << "Initializing SampleProducer with args : " << args;
			try
			{
				interval_ = stoi (args);
			}
			catch(...)
			{
				interval_ = 1;
			}
			if (interval_ < 0)
				interval_ = 1;
			state_ = false;
			LOG(INFO) << "SampleProducer is running with interval of " << interval_ << "s.";
			// Do more initialization here.
		}

		virtual void StartImplementation()
		{
			terminate_ = false;
			LOG(INFO) << "Running SampleProducer";
			while (!terminate_)
			{
				EventMessage message;
				message.set_binary_payload(state_);
				state_ = !state_;

				Publish(message);
				for (int ll = 0; ll < interval_ && !terminate_; ll++)
				{
					this_thread::sleep_for(std::chrono::seconds(1));
					if (terminate_)
						break;
				}
			}
		}

		virtual void TerminateImplementation()
		{
			LOG(INFO) << "Terminating SampleProducer";
			terminate_ = true;
		}

	private:
		bool terminate_;

		bool state_;
		int interval_;
};
extern "C" TardigradeModule* new_module()
{
	return new SampleProducer;
}

extern "C" void destroy_module(TardigradeModule* producer)
{
	delete producer;
}

