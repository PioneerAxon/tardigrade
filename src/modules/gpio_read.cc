#include <iostream>
#include <string>
#include <functional>
#include <chrono>

#include <glog/logging.h>

#include "producer.h"
#include "event-message.pb.h"
#include "gpio.h"

using namespace std;
using namespace tardigrade;
class GPIORead : public TardigradeProducer {

	public:
		virtual void InitializeImplementation(string args)
		{
			LOG(INFO) << "Initializing GPIORead with args : " << args;
			try
			{
				pin_ = stoi (args);
				CHECK(pin_ > 0 && pin_ <= 40);
			}
			catch(...)
			{
				LOG(FATAL) << "Unable to parse argument \"" << args << "\" to a physical pin number";
			}
			LOG(INFO) << "Registering physical pin " << pin_ << " as output device";
			terminate_ = false;
			pin_status_ = false;

			GPIO::SetPinMode(pin_, tInputPin);
			GPIO::SetInputPullMode(pin_, tPullDownMode);
		}

		virtual void StartImplementation()
		{
			LOG(INFO) << "Running GPIOReader";
			while (!terminate_)
			{
				if (GPIO::Read(pin_) != pin_status_)
				{
					pin_status_ = !pin_status_;
					EventMessage message;
					message.set_binary_payload(pin_status_);
					Publish(message);
				}
				this_thread::sleep_for(std::chrono::milliseconds(500));
			}
		}

		virtual void TerminateImplementation()
		{
			LOG(INFO) << "Terminating GPIORead";
			terminate_ = true;
		}
	private:
		bool terminate_;
		int pin_;
		bool pin_status_;
};
extern "C" TardigradeModule* new_module()
{
	return new GPIORead;
}

extern "C" void destroy_module(TardigradeModule* consumer)
{
	delete consumer;
}


