#ifndef __TARDIGRADE_PRODUCER_H__
#define __TARDIGRADE_PRODUCER_H__

#include <string>
#include <thread>

#include "event-message.pb.h"

#include "module.h"

namespace tardigrade
{
	class TardigradeProducer : public TardigradeModule
	{
		public:
			TardigradeProducer();
			~TardigradeProducer();

			void Start();

			ModuleType GetModuleType()
			{
				return ModuleType::tProducer;
			}

			virtual void InitializeImplementation(std::string arguments) = 0;
			virtual void TerminateImplementation() = 0;
			virtual void StartImplementation() = 0;

			void JoinWorkerThread();

			void Publish(const EventMessage& message);
			void Publish(const int index, const EventMessage& message);

		private:
			std::thread* worker_thread_;
	};
};


#endif //__TARDIGRADE_PRODUCER_H__
