#ifndef __TARDIGRADE_CONFIG_READER_H__
#define __TARDIGRADE_CONFIG_READER_H__

#include <list>

#include <yaml-cpp/yaml.h>

#include "module_config.h"

namespace tardigrade
{
	YAML::Emitter& operator<< (YAML::Emitter& out, const ModuleConfig& cfg);
	namespace ConfigReader
	{
		std::list<ModuleConfig>* Load();
	};
};

#endif //_TARDIGRADE_CONFIG_READER_H__
