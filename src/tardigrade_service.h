#ifndef __TARDIGRADE_SERVICE_H__
#define __TARDIGRADE_SERVICE_H__

#include "module_loader.h"

namespace tardigrade
{
	class Tardigrade
	{
		public:
			Tardigrade();
			~Tardigrade();
			void Run();
			static void SignalHandler(int signal);
			void Stop();

		private:
			ModuleLoader module_loader_;

		public:
			static bool caught_signal_;
	};
};

#endif //__TARDIGRADE_SERVICE_H__
