#include <stdlib.h>
#include <csignal>

#include "tardigrade_service.h"
#include "module_loader.h"
#include "module_config.h"
#include "config_reader.h"
#include "gpio.h"

namespace tardigrade
{
	bool Tardigrade::caught_signal_ = false;

	Tardigrade::Tardigrade()
	{
		std::signal(SIGINT, Tardigrade::SignalHandler);
		std::signal(SIGTERM, Tardigrade::SignalHandler);
	}

	Tardigrade::~Tardigrade()
	{
	}

	void Tardigrade::Run()
	{
		list<ModuleConfig>* configs = ConfigReader::Load();
		LOG_IF(ERROR, !configs) << "Configuration couldn't be loaded. Exiting gracefully.";
		if (configs)
		{
			GPIO::Init(tPhysicalMode);
			module_loader_.LoadAll(*configs);
			configs->clear();
			sigset_t interrupt_signal_set;
			sigemptyset(&interrupt_signal_set);
			while (!caught_signal_)
			{
				sigsuspend(&interrupt_signal_set);
			}
			Stop();
			LOG(INFO) << "Gracefully exiting.";
		}
		exit(0);
	}

	void Tardigrade::SignalHandler(int signal)
	{
		caught_signal_ = true;
	}

	void Tardigrade::Stop()
	{
		module_loader_.UnloadAll();
	}
};
