#include <iostream>

#include <glog/logging.h>
#include <gflags/gflags.h>

#include "tardigrade_monitor.h"

using namespace tardigrade;

int main(int argc, char* argv[])
{
	if (FLAGS_log_dir.empty())
		FLAGS_logtostderr = true;

	google::InitGoogleLogging(argv[0]);

	google::ParseCommandLineFlags(&argc, &argv, true);

	TardigradeMonitor monitor;
	monitor.Run();
	return 0;
}
