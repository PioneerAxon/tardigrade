#include <glog/logging.h>
#include <gflags/gflags.h>
#include <yaml-cpp/yaml.h>

#include <list>

#include "config_reader.h"
#include "module.h"
#include "module_config.h"

DEFINE_string(module_config_file_path, "./config.yaml", "Path of the module configuration file.");

using namespace std;
namespace tardigrade
{
	namespace ConfigReader
	{
		list<ModuleConfig>* Load()
		{

			list<ModuleConfig>* configs;

			YAML::Node config_file = YAML::LoadFile(FLAGS_module_config_file_path);
			CHECK(config_file.IsSequence());
			configs = new list<ModuleConfig>(config_file.as<list<ModuleConfig>>());
			return configs;
		}
	};

	using namespace YAML;
	Emitter& operator<< (Emitter& out, const tardigrade::ModuleConfig& config)
	{
		out << BeginMap;
		out << Key << "name" << Value << config.module_name_;
		out << Key << "arguments" << Value << config.arguments_;
		out << Key << "path" << Value << config.module_path_;
		out << Key << "type" << Value << static_cast<int>(config.type_);
		out << Key << "partition" << Value << config.partition_;
		out << Key << "namespace" << Value << config.namespace_;
		if (config.consumer_topics_.size() != 0)
		{
			out << Key << "consumes" << Value << config.consumer_topics_;
		}
		if (config.producer_topics_.size() != 0)
		{
			out << Key << "produces" << Value << config.producer_topics_;
		}
		out << EndMap;
		return out;
	}
};

namespace YAML
{
	using namespace tardigrade;
	template<> struct convert<ModuleConfig>
	{
		static Node encode(const tardigrade::ModuleConfig& rhs)
		{
			Node node;
			node["name"] = rhs.module_name_;
			node["path"] = rhs.module_path_;
			node["arguments"] = rhs.arguments_;
			node["type"] = static_cast<int>(rhs.type_);
			node["partition"] = rhs.partition_;
			node["namespace"] = rhs.namespace_;
			if (rhs.consumer_topics_.size() != 0)
			{
				node["consumes"] = rhs.consumer_topics_;
			}
			if (rhs.producer_topics_.size() != 0)
			{
				node["produces"] = rhs.producer_topics_;
			}
			return node;
		}

		static bool decode(const Node& node, tardigrade::ModuleConfig& rhs)
		{
			ModuleType type;
			if (!node.IsMap())
				return false;
			if (node["type"].as<string>().empty())
				return false;
			type = static_cast<tardigrade::ModuleType>(node["type"].as<int>());
			if (node["name"].as<string>().empty() || node["partition"].as<string>().empty() || node["namespace"].as<string>().empty())
				return false;

			rhs.module_name_ = node["name"].as<string>();
			rhs.SetPath(node["path"].as<string>());
			rhs.arguments_ = node["arguments"].as<string>();
			rhs.type_ = type;
			rhs.partition_ = node["partition"].as<string>();
			rhs.namespace_ = node["namespace"].as<string>();

			if (type == ModuleType::tProducer || type == ModuleType::tOperator)
			{
				rhs.producer_topics_ = node["produces"].as<vector<string>>();
			}
			if (type == ModuleType::tConsumer || type == ModuleType::tOperator)
			{
				rhs.consumer_topics_ = node["consumes"].as<vector<string>>();
			}
			return true;
		}
	};

};
