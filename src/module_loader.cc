#include <dlfcn.h>

#include <list>
#include <iostream>

#include "module.h"
#include "module_loader.h"
#include "module_config.h"

using namespace std;

namespace tardigrade
{
	ModuleLoader::ModuleLoader()
	{
	}

	ModuleLoader::~ModuleLoader()
	{
	}

	void ModuleLoader::LoadAll(const list<ModuleConfig>& modules)
	{
		for (list<ModuleConfig>::const_iterator it = modules.cbegin(); it != modules.cend(); it++)
		{
			TardigradeModule* module;
			module = LoadModule(*it);
			loaded_modules_.insert(loaded_modules_.end(), module);
		}
	}

	void ModuleLoader::UnloadAll()
	{
		for (list<TardigradeModule*>::iterator it = loaded_modules_.begin(); it != loaded_modules_.end(); it++)
		{
			UnloadModule(*it);
		}
		loaded_modules_.clear();
	}

	TardigradeModule* ModuleLoader::LoadModule(const ModuleConfig& config)
	{
		void* libdl_object = dlopen(config.module_path_.c_str(), RTLD_LAZY);
		CHECK(libdl_object) << "Can't load the module " << config.module_name_ << ". " << dlerror();

		new_module_t* new_module_func = (new_module_t*) dlsym(libdl_object, "new_module");
		CHECK(new_module_func) << "Couldn't load new_module method. " << dlerror();

		destroy_module_t* destroy_module_func = (destroy_module_t*) dlsym(libdl_object, "destroy_module");
		CHECK(destroy_module_func) << "Couldn't load destroy_module method. " << dlerror();

		TardigradeModule* module = new_module_func();

		LOG(INFO) << "Successfully loaded : " << config.module_name_;

		CHECK(module->GetModuleType() == config.type_) << "Module types didn't match.";

		CHECK(config.producer_topics_.size() > 0 || config.consumer_topics_.size() > 0);
		module->Initialize(libdl_object, config.arguments_, config.producer_topics_, config.consumer_topics_, config.namespace_, config.partition_);

		module->new_module_func_ = new_module_func;
		module->destroy_module_func_ = destroy_module_func;

		return module;
	}

	void ModuleLoader::UnloadModule(TardigradeModule* module)
	{
		void* libdl_object = module->Terminate();
		CHECK(libdl_object);
		CHECK(module->destroy_module_func_);
		module->destroy_module_func_(module);
		dlclose(libdl_object);
	}
};
