#ifndef __TARDIGRADE_CONSUMER_H__
#define __TARDIGRADE_CONSUMER_H__

#include <string>

#include "event-message.pb.h"

#include "module.h"

namespace tardigrade
{
	class TardigradeConsumer : public TardigradeModule
	{
		public:
			TardigradeConsumer();
			~TardigradeConsumer();

			ModuleType GetModuleType()
			{
				return ModuleType::tConsumer;
			}

			virtual void InitializeImplementation(std::string arguments) = 0;
			virtual void TerminateImplementation() = 0;
			virtual void Callback(const string& topic, const EventMessage& message) = 0;
	};
};

#endif //__TARDIGRADE_CONSUMER_H__
