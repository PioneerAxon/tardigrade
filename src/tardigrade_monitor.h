#ifndef __TARDIGRADE_MONITOR_H__
#define __TARDIGRADE_MONITOR_H__

namespace tardigrade
{

	class TardigradeMonitor
	{
		public:
			TardigradeMonitor();
			~TardigradeMonitor();
			void Run();
			static void SignalHandler(int signal);
		private:
			int RunService();
	};

};

#endif //__TARDIGRADE_MONITOR_H__
