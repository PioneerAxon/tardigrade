#include "module_config.h"

DEFINE_string(default_module_load_path, "./modules/", "Path to the directory containing modules.");
DEFINE_string(default_module_name_suffix, ".so", "Suffix of the module name that represents the module to be loaded.");

namespace tardigrade
{
	ModuleConfig::ModuleConfig(string module_name, string arguments, string partition, string _namespace, ModuleType type, string module_path) :
		module_name_(module_name),
		arguments_(arguments),
		type_(type),
		partition_(partition),
		namespace_(_namespace)
	{
		SetPath(module_path);
	}

	ModuleConfig::ModuleConfig() :
		module_name_(""),
		module_path_(""),
		arguments_(""),
		type_(ModuleType::tUnknown)
	{
	}

	ModuleConfig::~ModuleConfig()
	{
	}

	void ModuleConfig::SetPath(string path)
	{
		if (path.empty())
			module_path_ = FLAGS_default_module_load_path + "/" + module_name_ + FLAGS_default_module_name_suffix;
		else
			module_path_ = path + "/" + module_name_ + FLAGS_default_module_name_suffix;
	}
};
