TOP_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

all: $(TOP_DIR)/build
	@cd $(TOP_DIR)/build && cmake ../ $(ARGS) && $(MAKE) -s

$(TOP_DIR)/build:
	@mkdir $(TOP_DIR)/build

clean:
	rm -rf $(TOP_DIR)/build
